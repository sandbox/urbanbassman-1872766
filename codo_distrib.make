; Core version
; ------------
 
core = 7.x
  
; API version
; ------------
  
api = 2
  
; Modules
; --------
projects[admin][version] = 2.0-beta3
projects[admin][type] = "module"
projects[admin_menu][version] = 3.0-rc3
projects[admin_menu][type] = "module"
projects[module_filter][version] = 1.7
projects[module_filter][type] = "module"
projects[contemplate][version] = 1.0-rc3
projects[contemplate][type] = "module"
projects[ctools][version] = 1.2
projects[ctools][type] = "module"
projects[context][version] = 3.0-beta6
projects[context][type] = "module"
projects[calendar][version] = 3.4
projects[calendar][type] = "module"
projects[date][version] = 2.6
projects[date][type] = "module"
projects[devel][version] = 1.3
projects[devel][type] = "module"
projects[features][version] = 1.0
projects[features][type] = "module"
projects[content_taxonomy][version] = 1.0-beta1
projects[content_taxonomy][type] = "module"
projects[email][version] = 1.2
projects[email][type] = "module"
projects[filefield_paths][version] = 1.0-beta3
projects[filefield_paths][type] = "module"
projects[link][version] = 1.0
projects[link][type] = "module"
projects[flag][version] = 2.0
projects[flag][type] = "module"
projects[i18n][version] = 1.7
projects[i18n][type] = "module"
projects[og][version] = 2.0-beta4
projects[og][type] = "module"
projects[advanced_forum][version] = 2.0
projects[advanced_forum][type] = "module"
projects[advanced_help][version] = 1.0
projects[advanced_help][type] = "module"
projects[better_formats][version] = 1.0-beta1
projects[better_formats][type] = "module"
projects[entity][version] = 1.0-rc3
projects[entity][type] = "module"
projects[entityreference][version] = 1.0
projects[entityreference][type] = "module"
projects[feeds][version] = 2.0-alpha7
projects[feeds][type] = "module"
projects[gmap][version] = 1.0-beta1
projects[gmap][type] = "module"
projects[google_analytics][version] = 1.3
projects[google_analytics][type] = "module"
projects[libraries][version] = 2.0
projects[libraries][type] = "module"
projects[location][version] = 3.0-alpha1
projects[location][type] = "module"
projects[pathauto][version] = 1.2
projects[pathauto][type] = "module"
projects[scheduler][version] = 1.0
projects[scheduler][type] = "module"
projects[token][version] = 1.4
projects[token][type] = "module"
projects[panels][version] = 3.3
projects[panels][type] = "module"
projects[print][version] = 1.2
projects[print][type] = "module"
projects[rules][version] = 2.2
projects[rules][type] = "module"
projects[page_title][version] = 2.7
projects[page_title][type] = "module"
projects[wysiwyg][version] = 2.2
projects[wysiwyg][type] = "module"
projects[views][version] = 3.5
projects[views][type] = "module"
projects[views_bulk_operations][version] = 3.1
projects[views_bulk_operations][type] = "module"
projects[votingapi][version] = 2.10
projects[votingapi][type] = "module"
projects[webform][version] = 3.18
projects[webform][type] = "module"

  

; Themes
; --------

  
  
; Libraries
; ---------
libraries[jquery][download][type] = "file"
libraries[jquery][download][url] = "https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"
libraries[jqueryui][download][type] = "file"
libraries[jqueryui][download][url] = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"



